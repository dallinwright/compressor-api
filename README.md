# Compressor

Compressor is a small dummy nodejs rest API to do a pseudo compression algorithm when you post a string to it's api endpoint. The api endpoint when calling is `/api/compress`. It is to demonstrate a few key concepts.
 
1. NodeJS Express application that has been dockerized.
2. Functioning Unit tests testing 3 various test cases of usage of our 'API'.
3. Working compression function. Essentially, it will take a string and compress it like to a number which is the instances of the following character. For example, the string `cbdddd` would be compressed to 1c1b4d and returned in the API response.
4. Typescript static typing, git precommits, and more that you would typically encounter in development projects.

### Prerequisites

Docker installed on your local machine, if you wish to test.

### Installation

To install, simply git clone this directory, and run `docker-compose up`.

### Example of API in action

Here we can see the app running via docker-compose on a dev workstation.

![alt docker-compose](docker-compose.png)

Here we can see the API response from the `api/compress` endpoint.

![alt curl](curl.png)

### License

GPL V3

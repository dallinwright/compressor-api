import {INTERNAL_SERVER_ERROR, OK} from 'http-status-codes';
import {IResponse} from '../entities';

export const compress = (input: string): IResponse => {
  let finalString = '';

  try {
    let currentChar = input[0];
    let charCount = 1;
    let index = 1;

    while (index <= input.length) {
      if (input[index] === currentChar) {
        charCount += 1;
      } else {
        finalString += charCount + currentChar;
        currentChar = input[index];
        charCount = 1;
      }

      index += 1;
    }

    return {
      success: true,
      status: OK,
      message: 'compress API called',
      data: {
        compressedString: finalString,
      },
    };
  } catch (e) {
    return {
      success: false,
      status: INTERNAL_SERVER_ERROR,
      message: 'compress API called',
      data: {
        error: e,
      },
    };
  }
};

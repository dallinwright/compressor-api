export interface IResponse {
    success: boolean;
    status: number;
    message: string;
    data: object;
}

export class Response implements IResponse {
    public success: boolean;
    public status: number;
    public message: string;
    public data: object;

    constructor(
        success: boolean,
        status: number,
        message: string,
        data?: object,
    ) {
      this.success = success;
      this.status = status;
      this.message = message;
      this.data = data ? data : {};
    }
}

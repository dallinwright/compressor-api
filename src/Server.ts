import cookieParser from 'cookie-parser';
import express from 'express';
import {Request, Response} from 'express';
import logger from 'morgan';
import BaseRouter from './routes';

// Init express
const app = express();

// Add middleware/settings/routes to express.
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser());
app.use('/api', BaseRouter);

app.get('*', (req: Request, res: Response) => {
  res.status(500).send('ERROR');
});

export default app;

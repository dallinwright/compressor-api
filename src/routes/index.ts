import {Router} from 'express';
import CompressRouter from './Compress';

// Init router and path
const router = Router();

// Add sub-routes
router.use('/compress', CompressRouter);

// Export the base-router
export default router;

import {logger} from '../shared';
import {Request, Response, Router} from 'express';
import {BAD_REQUEST} from 'http-status-codes';
import {IResponse} from '../entities';
import {compress} from '../shared/Compress';

// Init shared
const router = Router();

// /******************************************************************************
//  *                       Add One - "POST /api/users/add"
//  ******************************************************************************/

router.post('/', async (req: Request, res: Response) => {
  try {
    logger.info(req);
    const {compressString} = req.body;

    if (!compressString) {
      return res.status(BAD_REQUEST).json({
        error: 'Missing pararameter in request.',
      });
    } else {
      const resp: IResponse = compress(compressString);

      return res.status(resp.status).json(resp);
    }
  } catch (err) {
    logger.error(err.message, err);
    return res.status(BAD_REQUEST).json({
      error: err.message,
    });
  }
});

export default router;

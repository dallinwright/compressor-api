import app from '../src/Server';

// Import the dependencies for testing
import chai from 'chai';
import chaiHttp from 'chai-http';

import 'mocha';

// Configure chai
chai.use(chaiHttp);
chai.should();
const expect = chai.expect;

describe('Compress API Tests', () => {

    const compressPath = '/api/compress';

    describe(`"POST:${compressPath}"`, () => {
        // Test to get all students record
        it('should compress aabbcc to 2a2b2c', (done) => {
            chai.request(app)
                .post(compressPath)
                .send({compressString: 'aabbcc'})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    expect(res.body.data).to.contain({compressedString: '2a2b2c'});
                    done();
                });
        });
    });

    describe(`"GET:${compressPath}"`, () => {
        // Test to get all students record
        it('should return a internal server error (500)', (done) => {
            chai.request(app)
                .get(compressPath)
                .end((err, res) => {
                    res.should.have.status(500);
                    done();
                });
        });
    });

    describe(`"PUT:${compressPath}"`, () => {
        // Test to get all students record
        it('should return a not found (404)', (done) => {
            chai.request(app)
                .put(compressPath)
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });
});

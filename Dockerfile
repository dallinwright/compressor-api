FROM node:alpine
MAINTAINER dallinwright01@gmail.com

COPY dist/src .

EXPOSE 3000
CMD [ "node", "." ]
